function cargarMapa() {
    let mapa = L.map('divMapa', { center: [4.625770, -74.172777], zoom: 6 });

    let mosaico = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 20,
        id: 'mapbox.streets'
    });
    mosaico.addTo(mapa);

   /* let unMarcador = L.marker([4.5973005493412105, -74.0749740600586]);
    unMarcador.addTo(mapa);*/

    var polygon = L.polygon([
        [ 4.573002645975919 , -74.17831420898436 ],
        [ 4.528511394139643 , -74.18312072753906 ],
        [ 4.470326401476538 , -74.10758972167969 ],
        [ 4.553837520857994 , -74.07875061035156 ],     
        [ 4.5969583310925755 ,-74.04853820800781 ],
        [ 4.620228796979006 ,-74.06089782714842  ],
        [ 4.646236060730802 , -74.04579162597655 ],
        [ 4.683876452449536 , -74.03343200683594 ],
        [ 4.735885292887776 , -74.01695251464844 ],  
        [ 4.797469648017672 , -74.02793884277344 ],
        [ 4.823470267503387 , -74.02450561523438 ],
              [ 4.8385228030120695,
              -74.07943725585938],   
             [  4.791311461314776,
              -74.0924835205078],
              [ 4.787205972686519,
              -74.11720275878906],
             [  4.73656959376131,
              -74.12818908691406 ],
             [  4.7215148182869315,
              -74.16664123535156 ],
             [  4.663345579805752,
              -74.17144775390625 ],
             [  4.653764300370555,
              -74.19685363769531 ],
             [  4.625019680565743,
               -74.22706604003906],
              [ 4.598327203100916,
               -74.18449401855469 ],             
              [4.573002645975919,
	     -74.17831420898436 ]
    ]).addTo(mapa);

    var polygon = L.polygon([
        [
              
            3.4938882258758985,-76.5194320678711
          ],
          [
            
            3.4596191400617515,-76.54415130615234
          ],
          [
            
            3.453450572660427,-76.57058715820312
          ],
          [
            
            3.442826834894567,-76.55994415283203
          ],
          [
            
            3.417466466330672,-76.5582275390625
          ],
          [
            
            3.369485560923647,-76.54243469238281
          ],
          [
            
            3.2975097774221243,-76.54380798339844
          ],
          [
            
            3.298195285826728,-76.5256118774414
          ],
          [
            
            3.353034418476744,-76.51702880859374
          ],
          [
            
            3.364344608719728,-76.50501251220703
          ],
          [
            
            3.3883354863358677,-76.5094757080078
          ],
          [
            
            3.4088986225131213,-76.48303985595703
          ],
          [
            
            3.400330702377271,-76.46106719970703
          ],
          [
            
            3.433916511825677,-76.4593505859375
          ],
          [
            
            3.44419764643449,-76.47857666015625
          ],
          [
            
            3.463046104607499,-76.47651672363281
          ],
          [
            
            3.4980004323211227,-76.4919662475586
          ],
          [
            
            3.4938882258758985,-76.5194320678711
          ]
    ]).addTo(mapa);
    /*let dosMarcador = L.marker([3.43151756447619, -76.51119232177734]);
    dosMarcador.addTo(mapa);*/

var polygon = L.polygon([
    [   
        10.451687958524097,-75.51383972167969,
      ],
      [
        
        10.424339015699848,-75.55709838867188,
      ],
      [
        
        10.393610795496638,-75.56465148925781
      ],
      [
        
        10.389558496715212,-75.54370880126953
      ],
      [
        
        10.39968914509817,-75.55194854736328
      ],
      [
        
        10.413871500496183,-75.54336547851562
      ],
      [
        
        10.405429699947703,-75.52345275878906
      ],
      [
        
        10.385506145385655,-75.5258560180664
      ],
      [
        
        10.36929621497792,-75.5093765258789
      ],
      [
        
        10.360515485530787,-75.50113677978516
      ],
      [
        
        10.366932196647943,-75.49083709716797
      ],
      [
        
        10.353085445367476,-75.48431396484375
      ],
      [
        
        10.378414404173913,-75.44723510742188
      ],
      [
       
        10.393610795496638, -75.45753479003906
      ],
      [
        
        10.40644272807687,-75.45032501220703
      ],
      [
        
        10.424001359408539,-75.46096801757812
      ],
      [
        
        10.408806447585796,-75.47504425048828
      ],
      [
        
        10.413196164861843,-75.49530029296875
      ],
      [
       
        10.419611794313038, -75.5148696899414
      ],
      [
        10.443584818816156,
        -75.50731658935547
      ],
      [
        10.451687958524097,-75.51383972167969
      ]
]).addTo(mapa);
   /* let tresMarcador = L.marker([10.418767640088701,  -75.52791595458984]);
    tresMarcador.addTo(mapa);
*/

    var cartaIcon = L.icon({
    iconUrl: '',
   //alto y ancho de la imagen 
    iconSize:     [60, 95], // size of the icon
   // ubicacion desde el punto 
    iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
   // punto desde el cual la ventana emergente debería abrir en relación con el iconoAnchor
    popupAnchor:  [-70, -76] // point from which the popup should open relative to the iconAnchor
});
L.marker([10.418767640088701,  -75.52791595458984], {icon: cartaIcon}).addTo(mapa);
var bogoIcon = L.icon({
    iconUrl: '',
   //alto y ancho de la imagen 
    iconSize:     [60, 60], // size of the icon
   // ubicacion desde el punto 
    iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
   // punto desde el cual la ventana emergente debería abrir en relación con el iconoAnchor
    popupAnchor:  [-70, -76] // point from which the popup should open relative to the iconAnchor
});
L.marker([4.5973005493412105, -74.0749740600586], {icon: bogoIcon}).addTo(mapa);
var caliIcon = L.icon({
    iconUrl: '',
    //alto y ancho de la imagen 
    iconSize:     [60, 90], // size of the icon
   // ubicacion desde el punto 
    iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
   // punto desde el cual la ventana emergente debería abrir en relación con el iconoAnchor
    popupAnchor:  [-70, -76] // point from which the popup should open relative to the iconAnchor
});
L.marker([3.43151756447619, -76.51119232177734], {icon: caliIcon}).addTo(mapa);

}
